package horseproblem;

import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Josep Borràs Sánchez & Bartomeu Ramis Tarragó
 */
public class Tauler extends JPanel{

    private int Costat;

    private int size;
    /*
    Marquen les dimensions del tauler en funció del nombre de files o columnes per la grandaria
    (pixels) que tenen cada un.
     */

    private Casella tauler[][];

    /*
    creació de l'array bidimensional del tauler.
     */
    public Tauler(int size) {
        this.setLayout(null);
        this.Costat = HorseProblem.WIDTH / size;
        this.setSize(size * Costat, size * Costat);
        this.size = size;
        tauler = new Casella[size][size];
        int b = 1;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (((i+j)% 2) == 0) {
                    tauler[i][j] = new Casella(0, j, i, Costat);
                } else {
                    tauler[i][j] = new Casella(1, j, i, Costat);
                }
                b++;
            }
        }
    }
    /*
    Pasant una x i una y ens dirà a quina casella del tauler pertany.
    */
    public Casella getCasella(int x, int y) {
        Casella aux = new Casella();
        int idx1 = 0, idx2 = 0;
        for (int i = 0; i <= size*Costat; i += Costat) {
            if ((x >= i) && (x <= i + Costat)) {
                for (int j = 0; j <= size*Costat; j += Costat) {
                    if ((y >= j) && (y <= j + Costat)) {
                        aux = tauler[idx2][idx1];
                    }
                    idx2++;
                }
            }
            idx1++;
        }
        return aux;
    }
    /*
    Recorrerà tot el tauler mirant casella a casella per veure si hi ha el cavall
    col·locat a alguna d'elles.
    */
    public boolean setHorse(){
        boolean horse = false;
        for(int i=0;i <size; i++){
            for(int j=0; j<size; j++){
                if(tauler[i][j].getHorse()){
                    horse = true;
                }
            }
        }
        return horse;
    }
    
    /*
    Mètode encarregat de pintar el tauler.
     */
    @Override
    public void paint(Graphics g) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                tauler[i][j].paintComponent(g);
            }
        }
    }
    
    public void clean(){
        for(int i=0;i<size;i++){
            for(int j=0; j<size;j++){
                tauler[i][j].setNum(0);
            }
        }
    }
    public Casella returncas(int i, int j){
        return tauler[i][j];
    }
}
