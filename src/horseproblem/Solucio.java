/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horseproblem;

import java.util.ArrayList;

/**
 *
 * @author JosepB
 */
public class Solucio {
    ArrayList<Casella> moviments;
    
    public Solucio(){
       this. moviments = null;
    }
    public Solucio(ArrayList<Casella> mov){
        this.moviments = new ArrayList<Casella>();
        for(int i=0;i<mov.size();i++){
            this.moviments.add(mov.get(i));
        }
    }
    
    
    public ArrayList<Casella> getSolucio(){
        return moviments;
    }
}
