package horseproblem;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Josep Borrás Sánchez & Bartomeu Ramis Tarragó
 */
public class Solucionari extends JPanel {
    
    private JLabel jltitol;
    private JLabel jltext;
    private JTextField input;
    private JTextArea output;
    private JScrollPane scroll;

    public Solucionari() {
        this.setLayout(null);
        this.setSize(150, HEIGHT + 44);
        this.setBackground(Color.LIGHT_GRAY);

        this.jltitol = new JLabel();
        this.jltitol.setText("         SOLUCIONS");
        add(this.jltitol);
        this.jltitol.setBounds(0, 0, 150, 30);

        this.output = new JTextArea();
        this.scroll = new JScrollPane(output);
        this.output.setBounds(0, 30, 150, 350);
        this.output.setEditable(false);
        this.output.setLineWrap(true);
        add(this.output);
        
        this.jltext = new JLabel();
        this.jltext.setText("Solució:");
        this.jltext.setBounds(10,385, 85,30);
        add(this.jltext);
        
        this.input = new JTextField();
        this.input.setBounds(60, 386, 42, 30);
        add(this.input);
        this.input.setEditable(false);
    }

    public void addSolucions(int i) {
        if(i<0){
            this.output.setText("No s'han trobat solucions");
        }else{
            String res="";
            for(int j=0; j<i; j++){
                res += "Solució "+(j+1)+"\n";
            }
            this.output.setText(res);
        }
        
        this.input.setEditable(true);
    }

    public void cleanSolucions(){
        this.output.setText("");
    }
    
    public int getInput(){
        return Integer.parseInt(this.input.getText());
    }
}
