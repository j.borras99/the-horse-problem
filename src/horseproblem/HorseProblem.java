package horseproblem;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 *
 * @author Josep Borràs Sánchez & Bartomeu Ramis Tarragó
 */
public class HorseProblem extends JFrame implements MouseListener {

    static final int WIDTH = 480;
    static final int HEIGHT = 480;

    private Casella cashorse;
    private Tauler tauler;
    private int n = 8;

    private Solucionari solucionari = new Solucionari();

    private ArrayList<Solucio> solucions = new ArrayList<Solucio>();
    private ArrayList<Casella> moviments = new ArrayList<Casella>();

    private JMenuBar jmbar;
    private JMenu jmwindows;
    private JMenu jmtauler;
    private JMenuItem jmireset;
    private JMenuItem jmi4x4;
    private JMenuItem jmi6x6;
    private JMenuItem jmi10x10;
    private JMenuItem jmi12x12;
    private JMenuItem jmialtres;
    private JMenuItem jmiexit;

    private JButton jbaccepta;

    public static void main(String[] args) {
        (new HorseProblem()).setVisible(true);
    }

    public HorseProblem() {
        this.setTitle("THE HORSE PROBLEM");
        this.setResizable(false);
        this.setSize(WIDTH + 150, HEIGHT + 44);
        this.setLayout(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.initComponents();
    }

    private void initComponents() {
        this.jmbar = new JMenuBar();
        this.jmwindows = new JMenu();
        this.jmtauler = new JMenu();
        this.jmireset = new JMenuItem();
        this.jmiexit = new JMenuItem();
        this.jmi4x4 = new JMenuItem();
        this.jmi6x6 = new JMenuItem();
        this.jmi10x10 = new JMenuItem();
        this.jmi12x12 = new JMenuItem();
        this.jmialtres = new JMenuItem();

        this.jmiexit.setText("Exit");
        this.jmireset.setText("Reset");
        this.jmi4x4.setText("4 x 4");
        this.jmi6x6.setText("6 x 6");
        this.jmi10x10.setText("10 x 10");
        this.jmi12x12.setText("12 x 12");
        this.jmialtres.setText("Altres");

        this.jmireset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evnt) {
                jmiResetActionPerformed(evnt);
            }
        });
        this.jmiexit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evnt) {
                jmiExitActionPerformed(evnt);
            }
        });
        this.jmi4x4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evnt) {
                jmi4x4ActionPerformed(evnt);
            }
        });
        this.jmi6x6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evnt) {
                jmi6x6ActionPerformed(evnt);
            }
        });
        this.jmi10x10.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evnt) {
                jmi10x10ActionPerformed(evnt);
            }
        });
        this.jmi12x12.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evnt) {
                jmi12x12ActionPerformed(evnt);
            }
        });
        this.jmialtres.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evnt) {
                jmiAltresActionPerformed(evnt);
            }
        });

        this.jmwindows.setText("Finsetra");
        this.jmwindows.add(this.jmireset);
        this.jmwindows.add(this.jmiexit);

        this.jmtauler.setText("Tauler");
        this.jmtauler.add(this.jmi4x4);
        this.jmtauler.add(this.jmi6x6);
        this.jmtauler.add(this.jmi10x10);
        this.jmtauler.add(this.jmi12x12);
        this.jmtauler.add(this.jmialtres);

        this.jmbar.add(this.jmwindows);
        this.jmbar.add(this.jmtauler);

        setJMenuBar(this.jmbar);

        this.tauler = new Tauler(n);
        this.tauler.addMouseListener(this);
        this.getContentPane().add(tauler);
        this.tauler.setBounds(0, 0, WIDTH, HEIGHT);

        this.getContentPane().add(solucionari);
        this.solucionari.setBounds(WIDTH, 0, 150, HEIGHT);

        this.jbaccepta = new JButton();
        this.jbaccepta.setText("Iniciar");
        this.solucionari.add(this.jbaccepta);
        this.jbaccepta.setBounds(50, 440, 85, 30);
        this.jbaccepta.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evnt) {
                jbAcceptaActionPerformed(evnt);
            }
        });
    }

    private void jbAcceptaActionPerformed(ActionEvent evn) {
        tauler.clean();
        repaint();
        Executar(solucionari.getInput() - 1);
    }

    /*
    Accions de la barra de menú. Reseteja el tauler o canviar les dimensions
    d'aquest per unes més grans o mes petites. Quan es reseteja el tauler es refa
    un tauler normal, de 8x8.
     */
    private void jmiResetActionPerformed(ActionEvent evnt) {
        n = 8;
        solucionari.cleanSolucions();
        solucions.clear();
        moviments.clear();
        cashorse = null;
        getContentPane().remove(this.tauler);
        tauler = new Tauler(n);
        tauler.addMouseListener(this);
        getContentPane().add(this.tauler);
        repaint();
    }

    private void jmi4x4ActionPerformed(ActionEvent evnt) {
        n = 4;
        solucionari.cleanSolucions();
        solucions.clear();
        moviments.clear();
        cashorse = null;
        getContentPane().remove(this.tauler);
        tauler = new Tauler(n);
        tauler.addMouseListener(this);
        getContentPane().add(this.tauler);
        repaint();
    }

    private void jmi6x6ActionPerformed(ActionEvent evnt) {
        n = 6;
        solucionari.cleanSolucions();
        solucions.clear();
        moviments.clear();
        cashorse = null;
        getContentPane().remove(this.tauler);
        tauler = new Tauler(n);
        tauler.addMouseListener(this);
        getContentPane().add(this.tauler);
        repaint();
    }

    private void jmi10x10ActionPerformed(ActionEvent evnt) {
        n = 10;
        solucionari.cleanSolucions();
        solucions.clear();
        moviments.clear();
        cashorse = null;
        getContentPane().remove(this.tauler);
        tauler = new Tauler(n);
        tauler.addMouseListener(this);
        getContentPane().add(this.tauler);
        repaint();
    }

    private void jmi12x12ActionPerformed(ActionEvent evnt) {
        n = 12;
        solucionari.cleanSolucions();
        solucions.clear();
        moviments.clear();
        cashorse = null;
        getContentPane().remove(this.tauler);
        tauler = new Tauler(n);
        tauler.addMouseListener(this);
        getContentPane().add(this.tauler);
        repaint();
    }

    /*
    Demana a l'usuari, a través d'un JOptionPane, de quines dimensions es vol el
    tauler. Per si vol una altre que no es doni directament.
     */
    private void jmiAltresActionPerformed(ActionEvent evnt) {
        solucionari.cleanSolucions();
        solucions.clear();
        moviments.clear();
        cashorse = null;
        n = Integer.parseInt(JOptionPane.showInputDialog(new JFrame(), "Dimensio del tauler \n(n x n): ", null));
        if (n > 1) {
            getContentPane().remove(this.tauler);
            tauler = new Tauler(n);
            tauler.addMouseListener(this);
            getContentPane().add(this.tauler);
            repaint();
        } else {
            Image img = null;
            try {
                img = ImageIO.read(new File("Imatges/icono.png"));
            } catch (IOException ioe) {
                ioe.getMessage();
            }
            Icon icono = new ImageIcon(img);
            JOptionPane.showMessageDialog(null, "El tauler no pot tenir\n menys de 2 caselles", "Ops!", JOptionPane.PLAIN_MESSAGE, icono);
        }
    }

    private void jmiExitActionPerformed(ActionEvent evnt) {
        System.exit(0);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        int x, y;
        if (!tauler.setHorse()) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                x = e.getX();
                y = e.getY();
                cashorse = tauler.getCasella(x, y);
                cashorse.setHorse(true);
                tauler.paintImmediately(cashorse.getRectangle());
            }

            Object[] options = {"SI", "NO"};
            int n = JOptionPane.showOptionDialog(new JFrame(), "Vols iniciar la cerca de la solució ?",
                    "Inici del problema", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                    null, options, options[0]);

            if (n == 0) {
                moviments.add(cashorse);
                assatjar_moviments(moviments, solucions, false, 3);               
                solucionari.addSolucions(solucions.size());
            } else {
                cashorse.setHorse(false);
                tauler.paintImmediately(cashorse.getRectangle());
            }
        }
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // No el controlarem
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // No el controlarem
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // No el controlarem
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // No el controlarem
    }

    private void assatjar_moviments(ArrayList<Casella> moviments, ArrayList<Solucio> solucions,
            boolean totes, int k) {
        //EL cavall ja ha pasat per totes les caselles
        if (moviments.size() == this.n * this.n) {
            solucions.add(new Solucio(moviments));
            return;
        }
        //Possibles moviments que pot realitzar el cavall
        int[][] possibles = {{1, 2}, {-1, 2}, {1, -2}, {-1, -2},
        {2, 1}, {-2, 1}, {2, -1}, {-2, -1}};

        for (int i = 0; i < possibles.length; i++) {
            Casella darreraposicio = moviments.get(moviments.size() - 1);

            int nX = darreraposicio.getI() + possibles[i][0];
            int nY = darreraposicio.getJ() + possibles[i][1];

            if (nX < 0 || nX >= this.n || nY < 0 || nY >= this.n) {
                //La casella no és del tauler.
                continue;
            }
            Casella novaposicio = tauler.returncas(nX, nY);

            if (moviments.contains(novaposicio)) {
                //No continuam per aquesta branca
                continue;
            }
            moviments.add(novaposicio);
            assatjar_moviments(moviments, solucions, totes, k);
            if (solucions.size() == k && !totes) {
                //S'han trobat totes les solucions demanades
                return;
            }
            moviments.remove(novaposicio);
        }
    }

    public void Executar(int k) {
        moviments = (solucions.get(k)).getSolucio();
        for (int i = 0; i < moviments.size(); i++) {
            moviments.get(i).setNum(i + 1);
        }
        repaint();
    }

}
