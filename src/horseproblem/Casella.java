package horseproblem;

import java.awt.Color;
import static java.awt.Color.BLACK;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Josep Borràs Sánchez & Bartomeu Ramis Tarragó
 */
public class Casella {

    private static final Color[] c = {Color.WHITE, Color.DARK_GRAY};

    private boolean horse = false;

    private Rectangle2D rec;
    private int x, y, i ,j;
    private int size;
    private String num;
    private Color color;

    public Casella() {
        /*
        Construcctor buit.
         */
    }

    public Casella(int i, int x, int y, int size) {
        this.color = c[i];
        this.x = x * size;
        this.y = y * size;
        this.i = y;
        this.j = x;
        this.size = size;
        this.num = "0";
        this.rec = new Rectangle2D.Float(this.x,this.y,this.size,this.size);
    }
    
    public int getI(){
        return this.i;
    }
    public int getJ(){
        return this.j;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public boolean getHorse() {
        return horse;
    }

    public void setHorse(Boolean flg) {
        horse = flg;
    }
    public void setNum(int num){
        this.num = ""+num;
    }
    public Rectangle getRectangle(){
        return rec.getBounds();
    }
    
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        if (!horse) {
            g.setColor(color);
            g2d.fill(rec);
        }else {
            try{
            g.setColor(Color.ORANGE);
            g2d.fill(rec);
            g.drawImage(ImageIO.read(new File("Imatges/icono.png")), this.x, this.y, this.size, this.size, null);
            }catch(IOException ioe){
                ioe.getMessage();
            }
        }if(num != "0"){
        g2d.setColor(BLACK);
        g.drawString(num,x+size/2,y+size/2);}
    }

    @Override
    public String toString(){
        return getI()+"|"+getJ();
    }
}
