/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horseproblem;

/**
 *
 * @author JosepB
 */
public class Operador {
    int []operador = new int[2];
    
    public Operador(int x, int y){
        operador[0] = x;
        operador[1] = y;
    }
    
    public int getOprX(){
        return operador[0];
    }
    
    public int getOprY(){
        return operador[1];
    }
}
