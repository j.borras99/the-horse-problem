Propous a recursive solution of the horse problem and queen problem, in a board
of n x n.
User can choose which problem want to resolve with a simple user interface.
In both solutions, the user put the first piece, then the program calculate, 
recursively, the solution of the choosen problem. If the problem hasn't 
solution, the program show a message that informs you.